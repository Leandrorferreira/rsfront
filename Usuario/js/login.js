let ap = document.querySelector('#campos  input:nth-child(1)');
let pass = document.querySelector('#campos  input:nth-child(2)');
let btnEntrar = document.querySelector('#btnEntrar');
let header = document.querySelector('header');
let popup = document.querySelector("#popup");
let popupTxt = document.querySelector("#popup p");
let btnOk = document.querySelector("#popup button");
let form = document.querySelector('form');

const gerarDadosForm = campos => { 
    let dados = {};
    
    for(let campo of campos){
        dados[campo.name] = campo.value;
    }    
    return dados;
}

const fazerLogin = () => {  
    const campos = document.querySelectorAll('form input');
    const dados = gerarDadosForm(campos);
    
    fetch('http://104.45.22.25/api/hallparty/login', {
    method: 'POST',
    headers: {
        'Content-type': 'application/json'
    },
    body: JSON.stringify(dados)
})
.then(resposta => resposta.json())
.then(respostaJson => {
    sessionStorage.setItem("usuario", JSON.stringify(respostaJson.usuario));
    sessionStorage.setItem("token", respostaJson.token);
    
    if(respostaJson.status == 403){
        popupTxt.innerHTML = "";
        popupTxt.innerHTML = "Apartamento e/ou Senha incorretos!";
        popup.style.display = 'flex';
    }
    else{
        window.location = 'menuPrincipal.html';
    }   
});
}

function validarCampos(ap, pass){   
    if(ap != "" && pass != ""){      
        return true;
    }
    else{
        return false;
    }    
}

btnEntrar.addEventListener("click", function(){  
    let camposValidados = validarCampos(ap.value, pass.value);
    header.style.backgroundColor = "rgba(10,23,55,0.5)";
    if(camposValidados){
        fazerLogin();
    }
    else{
        popupTxt.innerHTML = "Informe seu Ap. e senha!";
        popup.style.display = 'flex';
    } 
});

btnOk.addEventListener("click", function(){
    popup.style.display = 'none';
    header.style.backgroundColor = "#292961";
});
