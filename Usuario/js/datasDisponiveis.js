
//Data selecionada
let mes_s = '';
let ano_s = '';

const mostrarUsuarioLogado = () => {
  const secaoUsuario = document.querySelector('#secao-usuario');
  const usuarioString = sessionStorage.getItem("usuario");

  if(!usuarioString){
    window.location = 'login.html';
  }
 
  const usuario = JSON.parse(usuarioString);
  secaoUsuario.innerHTML = 'Usuário ' + usuario.userId;
}

mostrarUsuarioLogado();

$(document).ready(function() {
  var currentDate = new Date();
  
  //Dados vindo do banco de dados
  let dateList = ['2', '4', '6', '9'];
  let mes = '5';
  let ano = '2019';
 
  function generateCalendar(d) {
    function monthDays(month, year) {
      var result = [];
      var days = new Date(year, month, 0).getDate();
      for (var i = 1; i <= days; i++) {
        result.push(i);
      }
      return result;
    }
    Date.prototype.monthDays = function() {
      var d = new Date(this.getFullYear(), this.getMonth() + 1, 0);
      return d.getDate();
    };
    var details = {
      // totalDays: monthDays(d.getMonth(), d.getFullYear()),
      totalDays: d.monthDays(),
      weekDays: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
      months: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
    };
    var start = new Date(d.getFullYear(), d.getMonth()).getDay();
    var cal = [];
    var day = 1;
    for (var i = 0; i <= 6; i++) {
      cal.push(['<tr>']);
      for (var j = 0; j < 7; j++) {
        if (i === 0) {
          cal[i].push('<td>' + details.weekDays[j] + '</td>');
        } else if (day > details.totalDays) {
          cal[i].push('<td>&nbsp;</td>');                       
        } else {
          if (i === 1 && j < start) {
            cal[i].push('<td>&nbsp;</td>');
          } else {
            
            if(dateList.includes(day.toString()) && (d.getMonth()+1) == mes && d.getFullYear() == ano){      
              cal[i].push('<td class="dayMark">' + day++ + '</td>');                 
            }
            else{
              cal[i].push('<td class="day">' + day++ + '</td>');       
            }            
          }
        }
      }
      cal[i].push('</tr>');
    }
    cal = cal.reduce(function(a, b) {       
      return a.concat(b);
    }, []).join('');        
    

    $('table').append(cal);
    $('#month').text(details.months[d.getMonth()]);
    $('#year').text(d.getFullYear());
    $('td.day').mouseover(function() {
      $(this).addClass('hover');
    }).mouseout(function() {
      $(this).removeClass('hover');
    });
  }
  $('#left').click(function() {
    $('table').text('');
    if (currentDate.getMonth() === 0) {
      currentDate = new Date(currentDate.getFullYear() - 1, 11);
      generateCalendar(currentDate);
    } else {
      currentDate = new Date(currentDate.getFullYear(), currentDate.getMonth() - 1)
      generateCalendar(currentDate);
    }
  });
  $('#right').click(function() { 
    $('table').html('<tr></tr>');
    if (currentDate.getMonth() === 11) {
      currentDate = new Date(currentDate.getFullYear() + 1, 0);
      generateCalendar(currentDate);
    } else {
      currentDate = new Date(currentDate.getFullYear(), currentDate.getMonth() + 1)
      generateCalendar(currentDate);
    }
  });
  generateCalendar(currentDate);
  
  function abrirPopup(i){    
    //alert('Data: ' + i +'/' + (d.getMonth()+1) == mes + '/' +  d.getFullYear());
    alert(i);
    window.location = 'saloesDisponiveis.html';
  }
  
  //Pegar todos os TD com classe .day e adicionar função
  let days = document.querySelectorAll('.day');
 
  for(let d of days){  
    d.addEventListener("click", function(){
      abrirPopup($(d).text());
    });
  }
});


