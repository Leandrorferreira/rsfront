let popup = document.querySelector("#popup");
let popupTxt = document.querySelector("#popup p");
let btnPopOk = document.querySelector("#btnPopOk");
let btnPopCancelar = document.querySelector("#btnPopCancelar");
let btnCancelar = document.querySelectorAll("tbody tr td a");
let body = document.querySelector('body');


const mostrarUsuarioLogado = () => {
    const secaoUsuario = document.querySelector('#secao-usuario');
    const usuarioString = sessionStorage.getItem("usuario");
  
    if(!usuarioString){
      window.location = 'login.html';
    }
   
    const usuario = JSON.parse(usuarioString);
    secaoUsuario.innerHTML = 'Usuário ' + usuario.userId;
}

mostrarUsuarioLogado();


btnPopOk.addEventListener("click", function(){ 
    //Função para cancelar reserva

    popupTxt.innerHTML = "Cancelado com sucesso!"; 
    btnPopOk.style.display ='none';
    btnPopCancelar.style.display = 'none';
    
    setTimeout(fecharPopup, 1000);      
});

btnPopCancelar.addEventListener("click", function(){ 
    popupTxt.innerHTML = "";    
    popup.style.display = 'none';
     body.style.backgroundColor = '';
});

function chamarPopup(){
    body.style.backgroundColor = "rgba(10,23,55,0.5)";    
    popupTxt.innerHTML = "Deseja realmente cancelar a reserva?";
    popup.style.display = 'flex';
}

function fecharPopup(){
    popupTxt.innerHTML = ""; 
    popup.style.display = 'none';
    body.style.backgroundColor = '';
}

for(let btn of btnCancelar){   
    btn.onclick = chamarPopup;    
}