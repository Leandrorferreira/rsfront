
const mostrarUsuarioLogado = () => {
    const secaoUsuario = document.querySelector('#secao-usuario');
    const usuarioString = sessionStorage.getItem("usuario");
  
    if(!usuarioString){
      window.location = 'login.html';
    }
   
    const usuario = JSON.parse(usuarioString);
    secaoUsuario.innerHTML = 'Usuário ' + usuario.userId;
}

mostrarUsuarioLogado();
