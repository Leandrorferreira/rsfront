let btnConsultar = document.querySelectorAll('tbody tr td a');

for(let btn of btnConsultar){   
    btn.addEventListener("click", function(){   
        //Passar dados da linha senecionada    
        
        btn.href = "datasDisponiveis.html";
    });  
}

const mostrarUsuarioLogado = () => {
    const secaoUsuario = document.querySelector('#secao-usuario');
    const usuarioString = sessionStorage.getItem("usuario");
  
    if(!usuarioString){
      window.location = 'login.html';
    }
   
    const usuario = JSON.parse(usuarioString);
    secaoUsuario.innerHTML = 'Usuário ' + usuario.userId;
}

mostrarUsuarioLogado();
