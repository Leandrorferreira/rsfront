let data = document.querySelector('#data');
let section = document.querySelector('section');
let dtReservas = document.querySelector('#dtReservadas');
let titleDtReservas = document.querySelector('#dtReservadas article h2');
let btnReservar = document.querySelectorAll('tbody tr td a');
let popup = document.querySelector("#popup");
let popupTxt = document.querySelector("#popup p");
let btnPopOk = document.querySelector("#btnPopOk");
let btnPopCancelar = document.querySelector("#btnPopCancelar");
let body = document.querySelector('body');
let popupErro = document.querySelector('#popupErro');
let btnPopOkErr = document.querySelector('#btnPopOkErr');
let errTxt = document.querySelector('#popupErro p');

data.onchange = getData;
btnPopOkErr.onclick = fecharPopErr;

const mostrarUsuarioLogado = () => {
    const secaoUsuario = document.querySelector('#secao-usuario');
    const usuarioString = sessionStorage.getItem("usuario");
    
    if(!usuarioString){
        window.location = 'login.html';
    }
    
    const usuario = JSON.parse(usuarioString);
    secaoUsuario.innerHTML = 'Usuário ' + usuario.userId;
}

mostrarUsuarioLogado();

function getData(){
    
    let strData = data.value;
    let data_format = dataFormatada(strData);
    
    let dataValida = validarData(data_format);
    if(dataValida){ 
        buscarSalao();       
    }
    else{
        body.style.backgroundColor = "rgba(10,23,55,0.5)";    
        errTxt.innerHTML = "Informe uma data válida!";
        popupErro.style.display = 'flex';
    }
}

function dataFormatada(strData){ 
    let dia  = strData.substring(8, 10);
    let mes  = strData.substring(5, 7);  
    let ano = strData.substring(0, 4);    
    return dia+"/"+mes+"/"+ano;
}

function dataFormatadaBack(strData){ 
    let dia  = strData.substring(8, 10);
    let mes  = strData.substring(5, 7);  
    let ano = strData.substring(0, 4);    
    return ano+"-"+mes+"-"+dia;
}

function chamarPopup(){
    body.style.backgroundColor = "rgba(10,23,55,0.5)";    
    popupTxt.innerHTML = "Deseja confirmar a reserva?";
    popup.style.display = 'flex';
}

function fecharPopup(){
    popupTxt.innerHTML = ""; 
    popup.style.display = 'none';
    body.style.backgroundColor = '';
}

function fecharPopErr(){
    errTxt.innerHTML = ""; 
    popupErro.style.display = 'none';
    body.style.backgroundColor = '';
}

function validarData(strData){   
    // now = new Date
    // let dia = now.getDate();
    // let mes = now.getMonth() + 1;
    // if(mes < 10){
    //     mes = '0' + mes;
    // }
    
    // let ano = now.getFullYear();   
    // let data = dia + '/' + mes + '/' + ano;    
    
    if(strData){        
        return true;
    } 
    else{
        return false;
    }
}

for(let btn of btnReservar){   
    btn.onclick = chamarPopup;    
}

btnPopOk.addEventListener("click", function(){ 
    //Função para realizar reserva
    
    popupTxt.innerHTML = "Sua reserva foi efetuado!"; 
    btnPopOk.style.display ='none';
    btnPopCancelar.style.display = 'none';
    
    setTimeout(fecharPopup, 1000);      
});

btnPopCancelar.addEventListener("click", function(){ 
    popupTxt.innerHTML = "";    
    popup.style.display = 'none';
    body.style.backgroundColor = '';
});

    const buscarSalao = () => {       
        let dt = dataFormatadaBack(data.value);
      
        fetch('http://104.45.22.25/api/hallparty/salao/' + dt, {
        method: 'GET',
        headers: {
            'Content-type': 'application/json',
            'Authorization': 'Bearer ' + sessionStorage.getItem('token')
        },
        
    })
    .then(resposta => resposta.json())
    .then(dados => {   
        titleDtReservas.innerHTML = '';
        
        titleDtReservas.innerHTML = 'Salões Disponíveis para: ' + dataFormatada(data.value);
        section.style.display = 'none';
        dtReservas.style.display = 'block';
        
        listarSaloes(dados);
    })
    .catch(err => {
        console.log(err);
    });
}

const listarSaloes = saloes => {
    let table = document.querySelector('table tbody');
    
    for(let salao of saloes){
        table.innerHTML += `
        <tr>
        <td>${salao.nome}</td>
        <td>${salao.capacidade}</td>
        <td>${salao.valor}</td>
        <td><a href="#">Reservar</a></td>
        <tr>
        `;  
    }
}